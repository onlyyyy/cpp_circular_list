TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        CirList.cpp \
        main.cpp

HEADERS += \
    CirList.h
