#include <iostream>
#include"CirList.h"
using namespace std;
void menu();
int main()
{
    int choose;
    int index = 0,value = 0;
    int length = 0;
    CirList List;
    while(1)
    {
        menu();
        cout<<"enter your choose"<<endl;

        cin>>choose;
        switch (choose)
        {
            case 0:
                return 0;
            case 1:
                cout<<"enter the length"<<endl;
                cin>>length;
                List.create(length);
                break;
            case 2:
                cout<<"this list's length is "<<List.getsize()<<endl;
                break;
            case 3:
                if(List.isempty()==true)
                {
                    cout<<"this is a empty list"<<endl;
                }
                else
                {
                    cout<<"not a empty list"<<endl;
                }
                break;
            case 4:
                List.show();
                break;
            case 5:
                cout<<"enter the index"<<endl;
                cin>>index;
                cout<<"enter the value"<<endl;
                cin>>value;
                List.insert(index,value);
                break;
            case 6:
                cout<<"enter the value"<<endl;
                cin>>value;
                List.insert_first(value);
                break;
            case 7:
                cout<<"enter the value"<<endl;
                cin>>value;
                List.insert_end(value);
                break;
            case 8:
                cout<<"enter the index"<<endl;
                cin>>index;
                List.del(index);
                break;
            case 9:
                List.delete_first();
                break;
            case 10:
                List.delete_end();
                break;
            case 11:
                cout<<"enter the index you want to start"<<endl;
                cin>>index;

                List.circle(index);
                break;
            case 12:
                if(List.ifiscircle()==true)
                {
                    cout<<"this list have a circle"<<endl;
                }
                else
                {
                    cout<<"this list dont have a circle"<<endl;
                }
                break;
            default:
               cout<<"not a choice"<<endl;
                break;
        }
    }
}


void menu()
{

    cout<<endl<<endl;
    cout<<"C++ circle list"<<endl;
    cout<<"1.create a circle list"<<endl;
    cout<<"2.get length"<<endl;
    cout<<"3.if is empty"<<endl;
    cout<<"4.show this list"<<endl;
    cout<<"5.insert an Node"<<endl;
    cout<<"6.insert at head"<<endl;
    cout<<"7.insert at end"<<endl;
    cout<<"8.del an Node"<<endl;
    cout<<"9.del at head"<<endl;
    cout<<"10.del at end"<<endl;
    cout<<"11.insert in a circle"<<endl;
    cout<<"12.check if this list have a circle"<<endl;
    cout<<"0.exit"<<endl;
}
