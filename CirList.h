#ifndef CIRLIST_H
#define CIRLIST_H
#include<iostream>
using namespace std;
class Node
{
public:
    int data;
    Node * next;
};

class CirList
{
private:
    Node * head;

public:
    CirList();
    ~CirList();
    int getsize();
    bool isempty();
    void create(int length);
    void show();
    void insert(int index,int value);
    void insert_first(int value);
    void insert_end(int value);
    void del(int index);
    void delete_first();
    void delete_end();
    void get(int index);
    void circle(int index);
    bool ifiscircle();
};

#endif // CIRLIST_H
