#include"CirList.h"

CirList::CirList()
{
    head = nullptr;
}

CirList::~CirList()
{
    Node *p = head;
    Node * temp =p;
    if(temp==nullptr||temp->next==nullptr)
    {
        //空表
        return;
    }
    else
    {

        int length = getsize();
        for(int i=0;i<length;i++)
        {
            p =temp;
            temp = temp->next;
            delete p;
            p = nullptr;
        }
    }
}

int CirList::getsize()
{
    Node *temp = head;
    int length = 0;
    if(head==nullptr||head->next==nullptr)
    {
        return 0;
    }
    while(temp->next!=head)  //循环链表的尾节点会指向head
    {
        length++;
        temp = temp->next;
    }
    return length;

}

bool CirList::isempty()
{
    Node * temp=head;
    if(temp==nullptr||temp->next==nullptr)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void CirList::create(int length)
{
    if(length<=0)
    {
        cout<<"the length is illegal"<<endl;
        return;
    }
    head = new Node;
    Node * temp = head;//先指向链表头

    for(int i=0;i<length;i++)
    {
        cout<<"enter value of "<<i+1<<endl;
        Node * p = new Node;
        cin>>p->data;
        //p->next = head;
        temp->next = p;
        temp = p;
        p->next = head;
    }


}

void CirList::show()
{
    int length = getsize();
    if(length==0)
    {
        cout<<"this list is null"<<endl;
        return ;
    }
    Node * temp = head;
    while(temp->next!=head)//循环链表的尾指向了head
    {
        temp= temp->next;
        cout<<temp->data<<endl;
    }


}

void CirList::insert(int index, int value)
{
    int length = getsize();
    if(length ==0)
    {
        head = new Node;
        Node * p = new Node;
        head->next = p;
        p->data = value;
        p->next = head;
    }
    else if(index<length)
    {
        Node *temp =head;
        Node *p = new Node;
        for(int i=0;i<index;i++)
        {
            temp = temp->next;

        }
        p->data = value;
        p->next = temp->next;
        temp->next = p;
    }
    else
    {
        cout<<"index's value is illegal"<<endl;
        return ;
    }
}


void CirList::insert_first(int value)
{
    Node * p = new Node;
    p->data = value;
    if(getsize()==0)
    {
        //空表
        head = new Node;
        head->next = p;

        p->next = head;
    }
    else
    {
        p->next = head->next;
        head->next = p;
    }
}

void CirList::insert_end(int value)
{
    Node *p =new Node;
    p->data = value;
    if(getsize()==0)
    {
        //空表
        head = new Node;
        head->next = p;

        p->next = head;
    }
    else
    {
        Node * temp = head;
        while(temp->next!=head)//循环链表的最后一个节点指向的是head
        {
            temp = temp->next;
        }
        //在遍历到尾节点之后
        p->next = head;
        temp->next = p;
    }
}

void CirList::del(int index)
{
    if(index<=0||index>getsize())
    {
        cout<<"index value error"<<endl;
        return;
    }
    Node * p = head;
    Node *temp = p;
    for(int i=0;i<index-1;i++)
    {
        temp = p; //temp永远是p的前一个节点
        p = p->next;


    }
    temp->next = p->next;
    delete p;
    p = nullptr;

}

void CirList::delete_first()
{
    if(getsize()==0)
    {
        cout<<"this is an empty list"<<endl;
        return;
    }
    Node *p = head;
    Node * temp = head->next;
    p->next = p->next->next;
    delete temp;
    temp = nullptr;
}

void CirList::delete_end()
{
    if(getsize()==0)
    {
        cout<<"this is ane empty list"<<endl;
        return ;
    }
    Node * p = head->next;
    Node * temp = nullptr;
    while(p->next!=head)
    {
        temp = p;
        p = p->next;
    }
    delete p;
    temp->next = head;
    p = nullptr;


}

//不停地输入数据，实现类似定长循环链表的功能
void CirList::circle(int index)
{
    if(index>getsize()||index<=0)
    {
        cout<<"index value is error"<<endl;
        return ;
    }
    Node * temp = head;
    for(int i=0;i<index;i++)
    {
        temp = temp->next;
    }
    int value;
    while(1)
    {
        cout<<"enter the value or -999 to exit"<<endl;
        cin>>value;
        if(value == -999)
        {
            break;
        }
        else
        {
            temp->data = value;
            if(temp->next==head)
            {
                temp = temp->next->next;
            }
            else
            {
                temp = temp->next;
            }
        }
    }

}

//判断链表是否有环
bool CirList::ifiscircle()
{
    if(getsize()==0)
    {
        cout<<"this is an empty list"<<endl;
        return false;
    }
    Node * fast = head;
    Node * slow = head;
    while(fast->next!=head||slow->next!=head)
    {
        fast =fast->next->next;
        slow = slow->next;
        if(fast == slow)
        {
            return true;
        }
    }
    return false;
}
